INSERT INTO `store_lr3`.`address` (`id`, `city`) VALUES ('1', 'Moscow');
INSERT INTO `store_lr3`.`address` (`id`, `city`) VALUES ('2', 'St. Petersburg');
INSERT INTO `store_lr3`.`address` (`id`, `city`) VALUES ('3', 'Volgograd');

INSERT INTO `store_lr3`.`client` (`id`, `balance`, `credit_limit`, `discount`, `first_name`) VALUES ('1', '2000.00', '5000.00', '0.05', 'John');
INSERT INTO `store_lr3`.`client` (`id`, `balance`, `credit_limit`, `discount`, `first_name`) VALUES ('2', '10000.00', '0', '0.00', 'Peter');
INSERT INTO `store_lr3`.`client` (`id`, `balance`, `credit_limit`, `discount`, `first_name`) VALUES ('3', '0.00', '10000.00', '0.075', 'Stefan');

INSERT INTO `store_lr3`.`good` (`id`, `description`, `name`, `price`) VALUES ('1', 'desc1', 'screwdriver', '100.00');
INSERT INTO `store_lr3`.`good` (`id`, `description`, `name`, `price`) VALUES ('2', 'desc2', 'nails','5.00');
INSERT INTO `store_lr3`.`good` (`id`, `description`, `name`, `price`) VALUES ('3', 'desc3', 'hammer', '150.00');

INSERT INTO `store_lr3`.`manufacture` (`id`, `stocks`, `min_allowable_stocks`, `good_id`) VALUES ('1', '200', '100', '1');
INSERT INTO `store_lr3`.`manufacture` (`id`, `stocks`, `min_allowable_stocks`, `good_id`) VALUES ('2', '10', '50', '1');
INSERT INTO `store_lr3`.`manufacture` (`id`, `stocks`, `min_allowable_stocks`, `good_id`) VALUES ('3', '10000', '1000', '2');
INSERT INTO `store_lr3`.`manufacture` (`id`, `stocks`, `min_allowable_stocks`, `good_id`) VALUES ('4', '150', '100', '3');

INSERT INTO `store_lr3`.`address_has_client` (`address_id`, `client_id`) VALUES ('1', '1');
INSERT INTO `store_lr3`.`address_has_client` (`address_id`, `client_id`) VALUES ('2', '1');
INSERT INTO `store_lr3`.`address_has_client` (`address_id`, `client_id`) VALUES ('3', '2');
INSERT INTO `store_lr3`.`address_has_client` (`address_id`, `client_id`) VALUES ('1', '2');
INSERT INTO `store_lr3`.`address_has_client` (`address_id`, `client_id`) VALUES ('2', '3');

INSERT INTO `store_lr3`.`orders` (`id`, `date_complete`, `client_id`, `address_id`) VALUES ('1', '2022-01-04', '1', '1');
INSERT INTO `store_lr3`.`orders` (`id`, `date_complete`, `client_id`, `address_id`) VALUES ('2', '2022-03-22', '1', '2');
INSERT INTO `store_lr3`.`orders` (`id`, `date_complete`, `client_id`, `address_id`) VALUES ('3', '2022-08-31', '2', '1');
INSERT INTO `store_lr3`.`orders` (`id`, `date_complete`, `client_id`, `address_id`) VALUES ('4', '2022-09-01', '3', '3');
INSERT INTO `store_lr3`.`orders` (`id`, `date_complete`, `client_id`, `address_id`) VALUES ('5', '2022-05-22', '2', '1');
INSERT INTO `store_lr3`.`orders` (`id`, `date_complete`, `client_id`, `address_id`) VALUES ('6', '2021-12-09', '1', '2');

INSERT INTO `store_lr3`.`order_has_good` (`orders_id`, `good_id`, `good_amount`) VALUES ('1', '2', '10');
INSERT INTO `store_lr3`.`order_has_good` (`orders_id`, `good_id`, `good_amount`) VALUES ('1', '1', '1');
INSERT INTO `store_lr3`.`order_has_good` (`orders_id`, `good_id`, `good_amount`) VALUES ('2', '2', '50');
INSERT INTO `store_lr3`.`order_has_good` (`orders_id`, `good_id`, `good_amount`) VALUES ('3', '2', '14');
INSERT INTO `store_lr3`.`order_has_good` (`orders_id`, `good_id`, `good_amount`) VALUES ('4', '1', '1');
INSERT INTO `store_lr3`.`order_has_good` (`orders_id`, `good_id`, `good_amount`) VALUES ('5', '2', '5');
INSERT INTO `store_lr3`.`order_has_good` (`orders_id`, `good_id`, `good_amount`) VALUES ('6', '3', '2');
