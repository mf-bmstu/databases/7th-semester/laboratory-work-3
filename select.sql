-- Посчитать общее количество запасов для каждого товара
select good.name, sum(manufacture.stocks)
from good
left join manufacture on good.id = manufacture.good_id
group by good.id;

-- Вывести какие товары и сколько когда-либо заказал каждый клиент
select client.first_name, good.name, sum(order_has_good.good_amount)
from client
inner join orders on client.id = orders.client_id
inner join order_has_good on orders.id = order_has_good.orders_id
inner join good on order_has_good.good_id = good.id
where orders.date_complete is not null
group by client.id, good.id;

-- Сколько сэкономил каждый клиент с учётом текущей скидки клиента и всех его покупок
select client.first_name, good.name, sum(good.price*good_amount)  * client.discount
from client
inner join orders on client.id = orders.client_id
inner join order_has_good on orders.id = order_has_good.orders_id
inner join good on order_has_good.good_id = good.id
where orders.date_complete is not null
group by client.id;

-- Вывести названия товаров для которых запас на заводе не удволетворяет установленному минимуму.
select good.name, manufacture.id as manufacture_id, manufacture.stocks, manufacture.min_allowable_stocks
from good
left join manufacture on good.id = manufacture.good_id
where stocks < min_allowable_stocks;

-- Вывести количество клиентов закреплённых за каждым адресом
select address.city, count(address_has_client.client_id)
from address
left join address_has_client on address_has_client.address_id = address.id
group by address.id;




