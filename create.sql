DROP SCHEMA IF EXISTS store_lr3;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema store_lr3
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema store_lr3
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `store_lr3` DEFAULT CHARACTER SET utf8 ;
USE `store_lr3` ;

-- -----------------------------------------------------
-- Table `store_lr3`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `store_lr3`.`client` (
  `id` INT NOT NULL,
  `balance` DECIMAL(12,2) NOT NULL DEFAULT 0.00,
  `credit_limit` DECIMAL(12,2) NULL,
  `discount` DECIMAL(4,3) NULL DEFAULT 0.000,
  `first_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_lr3`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `store_lr3`.`address` (
  `id` INT NOT NULL,
  `city` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_lr3`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `store_lr3`.`orders` (
  `id` INT NOT NULL,
  `date_complete` DATE NULL,
  `client_id` INT NOT NULL,
  `address_id` INT NOT NULL,
  `client_discount` DECIMAL(5,4) NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_clients_idx` (`client_id` ASC) VISIBLE,
  INDEX `fk_orders_addresses1_idx` (`address_id` ASC) VISIBLE,
  CONSTRAINT `fk_orders_clients`
    FOREIGN KEY (`client_id`)
    REFERENCES `store_lr3`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_addresses1`
    FOREIGN KEY (`address_id`)
    REFERENCES `store_lr3`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_lr3`.`good`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `store_lr3`.`good` (
  `id` INT NOT NULL,
  `description` TEXT NULL,
  `name` VARCHAR(100) NOT NULL,
  `price` DECIMAL(12,2) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_lr3`.`manufacture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `store_lr3`.`manufacture` (
  `id` INT NOT NULL,
  `stocks` INT NULL,
  `min_allowable_stocks` INT NULL,
  `good_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_manufactures_goods1_idx` (`good_id` ASC) VISIBLE,
  CONSTRAINT `fk_manufactures_goods1`
    FOREIGN KEY (`good_id`)
    REFERENCES `store_lr3`.`good` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_lr3`.`order_has_good`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `store_lr3`.`order_has_good` (
  `orders_id` INT NOT NULL,
  `good_id` INT NOT NULL,
  `good_amount` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`orders_id`, `good_id`),
  INDEX `fk_orders_has_goods_goods1_idx` (`good_id` ASC) VISIBLE,
  INDEX `fk_orders_has_goods_orders1_idx` (`orders_id` ASC) VISIBLE,
  CONSTRAINT `fk_orders_has_goods_orders1`
    FOREIGN KEY (`orders_id`)
    REFERENCES `store_lr3`.`orders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_has_goods_goods1`
    FOREIGN KEY (`good_id`)
    REFERENCES `store_lr3`.`good` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `store_lr3`.`address_has_client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `store_lr3`.`address_has_client` (
  `address_id` INT NOT NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`address_id`, `client_id`),
  INDEX `fk_addresses_has_clients_clients1_idx` (`client_id` ASC) VISIBLE,
  INDEX `fk_addresses_has_clients_addresses1_idx` (`address_id` ASC) VISIBLE,
  CONSTRAINT `fk_addresses_has_clients_addresses1`
    FOREIGN KEY (`address_id`)
    REFERENCES `store_lr3`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_addresses_has_clients_clients1`
    FOREIGN KEY (`client_id`)
    REFERENCES `store_lr3`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
